<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Illuminate\Support\Facades\Log;
use DB;


class CrearUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usuarios = Usuario::all();

        // return view('usuarios.index', compact('$usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $datos)
    {

        //
        if (!$datos->foto_user) {
            $foto = $datos->foto_user;
        }
        else{
            $foto = 'user.png';
        }

        $user = new Usuario;
        $user->nombre = $datos['nombre'];
        $user->apellido = $datos['apellido'];
        $user->cedula = $datos['cedula'];
        $user->pais = $datos['pais'];
        $user->direccion = $datos['direccion'];
        $user->foto = $foto;
        $user1 = $user->save();

        if (!$datos->foto_user) {

            $file =  $datos->file('foto_user');
            $name = $file->getClientOriginalName();
            $extension = explode(".", $name);
            $fileName = $user->id.'.'.$extension[1];

            \Storage::disk('local')->put($fileName,  \File::get($file));

        }

        // return response()->json($data, $data['code']);
        return $user->id;
    }

    public function store(Request $request)
    {
        //
        if ($request->hasFile('imagen')) {
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
        }

        $usuario = new Usuario();
        $usuario->nombre = $request->input('nombre');
        $usuario->save();
        return 'Usuario guardado';

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
