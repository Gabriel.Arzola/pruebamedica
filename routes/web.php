<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('get_cliente','CrearUsuarioController');
// Route::post('create_user','CrearUsuarioController@create');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/prueba', function () {
    return view('welcome');
});


// Route::get('/nombre/{name}', function ($name) {
//     return 'welcome' . $name;
// });

Route::resource('/createUsuario','CrearUsuarioController@create');
Route::resource('/obtenerUsuario','CrearUsuarioController');



// Route::resource('crearUsuario', 'CrearUsuarioController');

// Rutas del Api

// Route::get('verUsuario', 'CrearUsuarioController')->name('verUsuario');
// Route::post('crearUsuario', 'CrearUsuarioController@create')->name('crearUsuario');


